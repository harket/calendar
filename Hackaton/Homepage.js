//Imaginary Data
var iname = "Juan de la Cruz"
var isubject = "Biology"
var iclasses = [[], [], ["Be", "Rb", "Sr"], ["Graviton", "Tau"]]

var subjectlist = [
	["Biology", "Chemistry", "Physics", "Math", "Integrated Science", "Earth Science"],
	["Social Science", "English", "Filipino"],
	["Art", "AdTech", "Computer Science"],
	["Research"]
]
var reqslist = [
	["Long Test", "Quiz", "Lab Experiment", "Lab Report", "Homework", "Seatwork"],
	["Long Test", "Quiz", "Project", "Homework", "Seatwork"],
	["Project", "Submission"],
	["ASwR", "LitRev", "Methodology", "Lab Experiment", "Lab Report", "Quiz"]
]

//_________________________________________
var today = new Date();
$(document).ready(function() {
	//Set up Sidebar
	$("#nameSpace").html(iname);
	$("#subjectSpace").html(isubject + " Teacher");
	for(var x=0; x<4; x++){
		if(subjectlist[x].indexOf(isubject)>=0){
			for(var y=0; y<reqslist[x].length; y++){
				var n = document.createElement("button");
				var node = document.createTextNode(reqslist[x][y]);
				n.appendChild(node);
				document.getElementById("sidebar").appendChild(n);
				var b = document.createElement("br");
				document.getElementById("sidebar").appendChild(b);
			}
		}
		else{}
	}

	$('#calendar').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,basicWeek,basicDay'
		},
		defaultDate: today,
		navLinks: true, // can click day/week names to navigate views
		editable: true,
		eventLimit: true, // allow "more" link when too many events
		events: [
			{
				title: 'All Day Event',
				//start: '2017-11-01'
			},
			{
				title: 'Long Event',
				start: '2017-11-07',
				end: '2017-11-10'
			},
			{
				id: 999,
				//title: 'Repeating Event',
				//start: '2017-11-09T16:00:00'
			},
		]
	});
	$('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
    //SUSPEND
    $('#NoClass').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
});