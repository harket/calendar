$(document).ready(function() {
	$('#calendar').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,basicWeek,basicDay'
		},
		defaultDate: today.getDate(),
		navLinks: true, // can click day/week names to navigate views
		editable: true,
		eventLimit: true, // allow "more" link when too many events
		events: [
			{
				title: 'All Day Event',
				//start: '2017-11-01'
			},
			{
				title: 'Long Event',
				//start: '2017-11-07',
				//end: '2017-11-10'
			},
			{
				id: 999,
				//title: 'Repeating Event',
				//start: '2017-11-09T16:00:00'
			},
		]
	});
	$('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
});