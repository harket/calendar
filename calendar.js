function retract()
{
	document.getElementById("sidebar").classList.remove("extend");
	document.getElementById("dayofweek").classList.remove("retract");
	document.getElementById("calendar").classList.remove("retract");
	
	document.getElementById("sidebar").classList.toggle("retract");
	document.getElementById("dayofweek").classList.toggle("extend");
	document.getElementById("calendar").classList.toggle("extend");
	document.getElementById("retract").removeEventListener("click", retract);
	document.getElementById("retract").addEventListener("click", extend);
	document.getElementById("samplereq").style.zIndex = "-1";
}
function extend()
{
	document.getElementById("sidebar").classList.remove("retract");
	document.getElementById("dayofweek").classList.remove("extend");
	document.getElementById("calendar").classList.remove("extend");
	
	document.getElementById("sidebar").classList.toggle("extend");
	document.getElementById("dayofweek").classList.toggle("retract");
	document.getElementById("calendar").classList.toggle("retract");
	document.getElementById("retract").removeEventListener("click", extend);
	document.getElementById("retract").addEventListener("click", retract);
	document.getElementById("samplereq").style.zIndex = "1";
}
document.getElementById("retract").addEventListener("click", retract);

var month = new Date();
var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
document.getElementById("dayofweek").innerHTML = months[month.getMonth()];
var year = new Date();
document.getElementById("dayofweek").innerHTML = document.getElementById("dayofweek").innerHTML + " " + year.getFullYear();
